# libarchive-rust

#### 介绍
本软件是使用Rust重写libarchive部分模块以修复一些高危漏洞的C和Rust混合版libarchive工具。
上游软件仓库是：https://gitee.com/openeuler/libarchive-rust

#### 软件架构
具体软件架构参见上游仓库。


#### 安装说明
1. rpmbuild -ba {path_to_spec}/libarchive-rust.spec  %将spec文件和tar.gz文件放在同一目录下，该命令会生成.rpm和.src.rpm包
2. yum install {path_to_rpm}/libarchive-rust.rpm  %使用改命令安装生成的.rpm包即可

#### 使用说明
参见上游仓库中使用说明。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
