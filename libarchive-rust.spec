#needsrootforbuild
%global __cargo_skip_build 0
%global aliasname libarchive

Name:           libarchive-rust
Version:        1.0
Release:        3
Summary:        The "libarchive-rust" program

License:        GPLv3+
URL:            https://gitee.com/openeuler/libarchive-rust
Source0:        %{name}-%{version}.tar.gz

BuildRequires:  gcc bison sharutils zlib-devel bzip2-devel xz-devel
BuildRequires:  lzo-devel e2fsprogs-devel libacl-devel libattr-devel
BuildRequires:  openssl-devel libxml2-devel lz4-devel automake libzstd-devel
BuildRequires:  autoconf libtool make
BuildRequires:  rust
BuildRequires:  cargo
BuildRequires:  g++
BuildRequires:  cmake
BuildRequires:  rust-packaging

%description
%{name} is an open-source BSD-licensed C programming library that 
provides streaming access to a variety of different archive formats,
including tar, cpio, pax, zip, and ISO9660 images. The distribution 
also includes bsdtar and bsdcpio, full-featured implementations of 
tar and cpio that use %{name}.

%package 	devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description 	devel
%{name}-devel contains the header files for developing
applications that want to make use of %{name}.

%package_help

%package -n bsdtar
Summary:        Manipulate tape archives
Requires:       %{name}%{?_isa} = %{version}-%{release}
 
%description -n bsdtar
The bsdtar package contains standalone bsdtar utility split off regular
libarchive packages.
 
 
%package -n bsdcpio
Summary:        Copy files to and from archives
Requires:       %{name}%{?_isa} = %{version}-%{release}
 
%description -n bsdcpio
The bsdcpio package contains standalone bsdcpio utility split off regular
libarchive packages.
 
 
%package -n bsdcat
Summary:        Expand files to standard output
Requires:       %{name}%{?_isa} = %{version}-%{release}
 
%description -n bsdcat
The bsdcat program typically takes a filename as an argument or reads standard
input when used in a pipe.  In both cases decompressed data it written to
standard output.

%prep
%autosetup -n %{name}-%{version} -p1

cp vendor rust/rust_ffi/ -rf
cp vendor rust/rust_project/ -rf
cd rust/rust_ffi
%cargo_prep
%cargo_generate_buildrequires

%build
sed -i '/\[source.crates-io\]/{n;d}' ./rust/rust_ffi/.cargo/config
sed -i '/\[source.local-registry\]/{n;d}' ./rust/rust_ffi/.cargo/config
sed -i '/\[source.local-registry\]/a directory = "vendor"' ./rust/rust_ffi/.cargo/config
cp ./rust/rust_ffi/.cargo ./ -rf
cp vendor/libz-sys/src/zlib-ng/tools/config.sub vendor/libz-sys/src/zlib-ng/tools/config.sub.bak
autoreconf -ifv
%configure --disable-rpath
#sed -i 's/3006001/3000000/' build/version #avoid so confict
rm -rf vendor/libz-sys/src/zlib-ng/tools/config.sub
mv vendor/libz-sys/src/zlib-ng/tools/config.sub.bak vendor/libz-sys/src/zlib-ng/tools/config.sub
rm -rf CMakeCache.txt
make clean
cmake -DSTEP="build" -DCMAKE_INSTALL_PREFIX=/usr
make
cd rust
cargo clean
cargo build --release
cd ../
cmake -DSTEP="link" -DCMAKE_INSTALL_PREFIX=/usr
make

%install
%make_install
%delete_la
rm -rf %{buildroot}/usr/lib/%{aliasname}.a
mv %{buildroot}/usr/lib %{buildroot}/usr/lib64

%define _unpackaged_files_terminate_build 1

replace ()
{
    filename=$1
    file=`basename "$filename"`
    binary=${file%%.*}
    pattern=${binary##bsd}

    awk "
        # replace the topic
        /^.Dt ${pattern^^} 1/ {
            print \".Dt ${binary^^} 1\";
            next;
        }
        # replace the first occurence of \"$pattern\" by \"$binary\"
        !stop && /^.Nm $pattern/ {
            print \".Nm $binary\" ;
            stop = 1 ;
            next;
        }
        # print remaining lines
        1;
    " "$filename" > "$filename.new"
    mv "$filename".new "$filename"
}

for manpage in bsdtar.1 bsdcpio.1
do
    installed_manpage=`find "$RPM_BUILD_ROOT" -name "$manpage"`
    replace "$installed_manpage"
done

%check
%if %{with check}
logfiles ()
{
    find -name '*_test.log' -or -name test-suite.log
}

tempdirs ()
{
    cat `logfiles` \
        | awk "match(\$0, /[^[:space:]]*`date -I`[^[:space:]]*/) { print substr(\$0, RSTART, RLENGTH); }" \
        | sort | uniq
}

cat_logs ()
{
    for i in `logfiles`
    do
        echo "=== $i ==="
        cat "$i"
    done
}

run_testsuite ()
{
    rc=0
    %make_build check -j1 || {
        # error happened - try to extract in koji as much info as possible
        cat_logs

        for i in `tempdirs`; do
            if test -d "$i" ; then
                find $i -printf "%p\n    ~> a: %a\n    ~> c: %c\n    ~> t: %t\n    ~> %s B\n"
                cat $i/*.log
            fi
        done
        return 1
    }
    cat_logs
}

run_testsuite

%endif

%files
%defattr(-,root,root)
%{!?_licensedir:%global license %%doc}
%license COPYING
%{_libdir}/%{aliasname}.so.19*

%files devel
%defattr(-,root,root)
%{_includedir}/*.h
%{_libdir}/%{aliasname}.so
%{_libdir}/pkgconfig/%{aliasname}.pc

%files help
%defattr(-,root,root)
%doc NEWS README.md
%{_mandir}/man1/*
%{_mandir}/man3/*
%{_mandir}/man5/*

%files -n bsdtar
%{!?_licensedir:%global license %%doc}
%license COPYING
%doc NEWS README.md
%{_bindir}/bsdtar
 
%files -n bsdcpio
%{!?_licensedir:%global license %%doc}
%license COPYING
%doc NEWS README.md
%{_bindir}/bsdcpio
 
%files -n bsdcat
%{!?_licensedir:%global license %%doc}
%license COPYING
%doc NEWS README.md
%{_bindir}/bsdcat

%changelog
* Fri Dec 16 2022 seuzw <930zhaowei@163.com> - 1.0-3
- package other rpms

* Mon Dec 12 2022 seuzw <930zhaowei@163.com> - 1.0-2
- package files to rpm

* Mon Dec 12 2022 seuzw <930zhaowei@163.com> - 1.0-2
- add local source to solve dependency issues

* Tue Nov 29 2022 Kun Cheng <779834766@qq.com> - 1.0-1
- BuildRequires: add cmake
